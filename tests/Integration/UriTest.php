<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\UriIntegrationTest;
use Intec\MovaPsr7\Uri;

class UriTest extends UriIntegrationTest
{
    public function createUri($uri)
    {
        return new Uri($uri);
    }
}
