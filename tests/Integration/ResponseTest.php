<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\ResponseIntegrationTest;
use Intec\MovaPsr7\Response;

class ResponseTest extends ResponseIntegrationTest
{
    public function createSubject()
    {
        return new Response();
    }
}
