<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\StreamIntegrationTest;
use Intec\MovaPsr7\Stream;

class StreamTest extends StreamIntegrationTest
{
    public function createStream($data)
    {
        return Stream::create($data);
    }
}
