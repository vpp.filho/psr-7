<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\ServerRequestIntegrationTest;
use Intec\MovaPsr7\ServerRequest;

class ServerRequestTest extends ServerRequestIntegrationTest
{
    public function createSubject()
    {
        $_SERVER['REQUEST_METHOD'] = 'GET';

        return new ServerRequest('GET', '/', [], null, '1.1', $_SERVER);
    }
}
