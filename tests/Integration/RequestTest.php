<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\RequestIntegrationTest;
use Intec\MovaPsr7\Request;

class RequestTest extends RequestIntegrationTest
{
    public function createSubject()
    {
        return new Request('GET', '/');
    }
}
