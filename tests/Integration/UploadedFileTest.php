<?php

namespace Tests\Intec\MovaPsr7\Integration;

use Http\Psr7Test\UploadedFileIntegrationTest;
use Intec\MovaPsr7\Factory\Psr17Factory;
use Intec\MovaPsr7\Stream;

class UploadedFileTest extends UploadedFileIntegrationTest
{
    public function createSubject()
    {
        return (new Psr17Factory())->createUploadedFile(Stream::create('writing to tempfile'));
    }
}
